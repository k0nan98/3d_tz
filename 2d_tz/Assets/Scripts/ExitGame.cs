﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGame : MonoBehaviour
{
    public void QuitGame()
    {
        if(Application.loadedLevel != 0)
        {
            Application.LoadLevel(0);
        } else
        {
            Application.Quit();
        }
    }
}
