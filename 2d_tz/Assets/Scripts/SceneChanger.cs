﻿using Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    void SwitchScene()
    {
        if (GameResult.win)
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        } else
        {
            Application.LoadLevel(Application.loadedLevel);
        }
    }
}
