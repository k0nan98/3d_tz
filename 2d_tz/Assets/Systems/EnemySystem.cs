using Leopotam.Ecs;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using System;

namespace Client {
    sealed class EnemySystem : IEcsInitSystem {
        // auto-injected fields.
        readonly EcsWorld _world = null;
        CompositeDisposable disposables;
        public void Init()
        {
            disposables = new CompositeDisposable();
            GameObject[] enemys = GameObject.FindGameObjectsWithTag("Enemy");
            foreach(GameObject enemy in enemys)
            {
                enemy.OnCollisionEnter2DAsObservable().Subscribe<Collision2D>(x => { CollisionDetection(x); }).AddTo(disposables);
                
            }
        }
        void CollisionDetection(Collision2D col)
        {
            if(col.gameObject.tag == "Player")
            {
                GameResult.setResult(false);
            }
        }


    }
}