using Leopotam.Ecs;
using UnityEngine;

namespace Client {
    sealed class GameResult {
        // auto-injected fields.
        readonly EcsWorld _world = null;
        public static bool win = false;
        public static void setResult(bool state)
        {
            win = state;
            switch (win)
            {
                case true:
                    GameObject.Find("WinCanvas").GetComponent<Animator>().SetTrigger("Show");
                    break;
                case false:
                    GameObject.Find("LooseCanvas").GetComponent<Animator>().SetTrigger("Show");
                    break;
            }
        }
    }
}