using Leopotam.Ecs;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace Client {
    sealed class GateSystem : IEcsInitSystem
    {
        // auto-injected fields.
        readonly EcsWorld _world = null;
        CompositeDisposable disposables;
        public void Init()
        {
            disposables = new CompositeDisposable();
            GameObject gate = GameObject.FindGameObjectWithTag("Gate");

                gate.OnCollisionEnter2DAsObservable().Subscribe<Collision2D>(x => { CollisionDetection(x); }).AddTo(disposables);
        }
        void CollisionDetection(Collision2D col)
        {
            if (col.gameObject.tag == "Player")
            {
                GameResult.setResult(true);
            }
        }


    }
}