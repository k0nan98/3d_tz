using Leopotam.Ecs;
using UnityEngine;
namespace Client {
    sealed class SwipeSystem : IEcsRunSystem {
        // auto-injected fields.
        readonly EcsWorld _world = null;
        Vector3 firstPressPos, secondPressPos, currentSwipe;
        
        void IEcsRunSystem.Run()
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                //save began touch 2d point
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }
            if (t.phase == TouchPhase.Ended)
            {
                //save ended touch 2d point
                secondPressPos = new Vector2(t.position.x, t.position.y);

                //create vector from the two points
                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                //normalize the 2d vector
                currentSwipe.Normalize();
                GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().AddForce(new Vector2(currentSwipe.x, currentSwipe.y), ForceMode2D.Impulse);
            }
        }
    }
}