﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
public class FaceBookManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        FB.Init();
        DontDestroyOnLoad(this.gameObject);
    }
    public void LogLevelEvent(int level)
    {
        FB.LogAppEvent("LevelComplete:" + level);
    }

}
