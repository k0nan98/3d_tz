using Leopotam.Ecs;
using UnityEngine;

namespace Client {
    sealed class SampleSceneController : MonoBehaviour {
        EcsWorld _world;
        EcsSystems _systems;

        void Start () {
            // void can be switched to IEnumerator for support coroutines.
            
            _world = new EcsWorld ();
            _systems = new EcsSystems (_world);
#if UNITY_EDITOR
            Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create (_world);
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create (_systems);
#endif
            _systems
                .Add(new GunMoving())
                .Add(new MouseRaycast())
                .Add(new RocketFlySystem())
                .Add(new GunFire())
                .Add(new LevelUI())
                .Add(new BulletsCounter())
                .Add(new PointsCounter())
                .Add(new EndLevelSystem())
                .Init ();
        }

        void Update () {
            _systems?.Run ();
        }

        void OnDestroy () {
            if (_systems != null) {
                _systems.Destroy ();
                _systems = null;
                _world.Destroy ();
                _world = null;
            }
            
        }
    }
}