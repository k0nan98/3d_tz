﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockDestroyer : MonoBehaviour
{
    private void FixedUpdate()
    {
        if(this.transform.position.y < 0)
        {
            Destroy(this.gameObject);
        }
    }
}
