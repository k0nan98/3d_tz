﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(this.tag == "Rocket")
        this.transform.parent = null;
        GameObject.Find("GunParticle").GetComponent<ParticleSystem>().Stop();
    }

    // Update is called once per frame
    void Update()
    {

        if (this.tag == "Rocket")
            this.GetComponent<Rigidbody>().AddForce(this.transform.forward * 4, ForceMode.Acceleration);
            
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        //this.GetComponent<Rigidbody>().AddExplosionForce(currentSettings.GetRocketExplosionForce(), this.transform.position, currentSettings.GetRocketExplosionRadius());
        UnityEngine.GameObject.Destroy(this.gameObject);
    }
}
