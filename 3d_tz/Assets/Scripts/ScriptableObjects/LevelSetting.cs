﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newLevelData", menuName = "LevelData", order = 51)]
public class LevelSetting : ScriptableObject
{
    [SerializeField]
    private string LevelString;
    [SerializeField]
    private float rocketExplosionForce;
    [SerializeField]
    private float rocketExplosionRadius;
    public string GetLevelString()
    {
        return LevelString;
    }
    public float GetRocketExplosionForce()
    {
        return rocketExplosionForce;
    }
    public float GetRocketExplosionRadius()
    {
        return rocketExplosionRadius;
    }
}
