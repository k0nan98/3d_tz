﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Client;
public class ExitGame : MonoBehaviour
{
    public void ToMenu()
    {
        GunFire.fireNumber = 0;
        PointsCounter.points = 0;
        Application.LoadLevel(0);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
