﻿using Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadNextLevel : MonoBehaviour
{
    public void LoadNext()
    {
        GunFire.fireNumber = 0;
        PointsCounter.points = 0;
        Application.LoadLevel(Application.loadedLevel + 1);
    }

}
