﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Client;
public class ReloadLevel : MonoBehaviour
{
    public void LoadNext()
    {
        GunFire.fireNumber = 0;
        PointsCounter.points = 0;
        Application.LoadLevel(Application.loadedLevel);
    }
}
