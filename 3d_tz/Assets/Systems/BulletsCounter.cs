using Leopotam.Ecs;
using UnityEngine;
namespace Client {
    sealed class BulletsCounter : IEcsRunSystem, IEcsInitSystem {
        // auto-injected fields.
        readonly EcsWorld _world = null;
        int maxBullets = 0;
        public static int bullets = 0;
        public void Init()
        {
            maxBullets = (Application.loadedLevel) * 3;
        }

        void IEcsRunSystem.Run () {
            bullets = maxBullets - (int)GunFire.fireNumber;
            // add your run code here.
        }
    }
}