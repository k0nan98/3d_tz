using Leopotam.Ecs;
using UnityEngine;
using UniRx;
namespace Client
{
    sealed class EndLevelSystem : IEcsRunSystem
    {
        // auto-injected fields.
        readonly EcsWorld _world = null;
        GameObject gameCanvas = null;
        CompositeDisposable disposables;
        void IEcsRunSystem.Run()
        {
            if (BulletsCounter.bullets == 0 )
            {
                if (PointsCounter.GetGameResult() && gameCanvas == null) {
                    GameObject facebook = GameObject.Find("FaceBookManager");
                    facebook.GetComponent<FaceBookManager>().LogLevelEvent(Application.loadedLevel);
                    gameCanvas = GameObject.Find("WinCanvas");
                    gameCanvas.GetComponent<Animator>().SetTrigger("ToNextLevel");
                } else
                {
                    if (disposables == null)
                    {
                        disposables = new CompositeDisposable();
                        gameCanvas = GameObject.Find("LooseCanvas");
                        Observable.Timer(System.TimeSpan.FromSeconds(5))
    .Single()
    .Subscribe(_ => {
        ReloadLevel();
    }).AddTo(disposables);
                    }
                    
                }
            }

        }
        void ReloadLevel()
        {
            if(PointsCounter.GetGameResult())
            {
                GameObject facebook = GameObject.Find("FaceBookManager");
                facebook.GetComponent<FaceBookManager>().LogLevelEvent(Application.loadedLevel);
                gameCanvas = GameObject.Find("WinCanvas");
                gameCanvas.GetComponent<Animator>().SetTrigger("ToNextLevel");
                return;
            }
            gameCanvas = GameObject.Find("LooseCanvas");
            gameCanvas.GetComponent<Animator>().SetTrigger("ToNextLevel");
        }
        public void Destroy()
        {
            if (disposables != null)
            {
                disposables.Dispose();
            }
        }
    }
}