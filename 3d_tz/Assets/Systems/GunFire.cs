using Leopotam.Ecs;
using UnityEngine;
using UniRx;
using System.Net;

namespace Client {
    sealed class GunFire : IEcsRunSystem, IEcsDestroySystem {
        readonly EcsWorld _world = null;
        public static uint fireNumber = 0;
        CompositeDisposable disposables;
        bool canFire = true;

        void SetCanFire()
        {
            if(BulletsCounter.bullets > 0)
            canFire = true;
        }
        void IEcsRunSystem.Run() {
            if (disposables == null)
            {
                disposables = new CompositeDisposable();
            }



#if UNITY_EDITOR
            
                if (Input.GetKeyDown(KeyCode.Mouse0) && canFire){
                fireNumber++;
                canFire = false;
                
                RocketFlySystem.StartRocket();
                Observable.Timer(System.TimeSpan.FromSeconds(2))
.Single()
.Subscribe(_ => {
SetCanFire();

}).AddTo(disposables);
            }

#elif UNITY_ANDROID
            if (Input.touchCount > 0){
                                if(Input.GetTouch(0).phase == TouchPhase.Began)
            {
                GameObject.Find("GunParticle").GetComponent<ParticleSystem>().Play();
                
            }
            if(Input.GetTouch(0).phase == TouchPhase.Ended && canFire)
            {

            fireNumber++;
                canFire = false;
                RocketFlySystem.StartRocket();
                GameObject.Find("GunParticle").GetComponent<ParticleSystem>().Stop();
                       Observable.Timer(System.TimeSpan.FromSeconds(2))
.Single()
.Subscribe(_ => {
    SetCanFire();

}).AddTo(disposables);
            }
                }
#endif

        }

        public void Destroy()
        {
            if(disposables != null)
            {
                disposables.Dispose();
            }
        }
    }
}