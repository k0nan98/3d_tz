using Leopotam.Ecs;
using System;
using UnityEngine;
namespace Client {
    sealed class GunMoving : IEcsInitSystem, IEcsRunSystem
    {
        const string GunTag = "Gun";
        readonly EcsWorld _world = null;

        GameObject Gun = null;
        void IEcsInitSystem.Init()
        {
            Gun = GameObject.FindGameObjectWithTag(GunTag);
        }
        void IEcsRunSystem.Run () {
            if (Gun != null)
            {
                if (MouseRaycast.HitPosition != null)
                {
                        Vector3 corrected = MouseRaycast.HitPosition;
                        corrected.y = Mathf.Clamp(corrected.y, 0.5f, 3);
                    //Debug.Log(corrected.y);    
                    Gun.transform.LookAt(corrected);
                }
            }
            
        }

    }
}