using Leopotam.Ecs;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Client {
    sealed class LevelUI : IEcsRunSystem, IEcsInitSystem {
        // auto-injected fields.
        readonly EcsWorld _world = null;
        LevelSetting currentSettings;
        GameObject LevelText;
        GameObject bulletsText;
        GameObject Points3dText;
        GameObject CompleteSlider;
        public void Init()
        {
            LevelText = GameObject.Find("Level");
            bulletsText = GameObject.Find("Bullets");
            Points3dText = GameObject.Find("Points3dText");
            CompleteSlider = GameObject.Find("CompleteSlider");
        }

        void IEcsRunSystem.Run () {
            
            currentSettings = Resources.Load<LevelSetting>("Level_" + (Application.loadedLevel));
            LevelText.GetComponent<TextMeshProUGUI>().text = "Level: " + (currentSettings.GetLevelString());
            bulletsText.GetComponent<TextMeshProUGUI>().text = "Bullets: " + BulletsCounter.bullets;
            Points3dText.GetComponent<TextMeshPro>().text = string.Empty + PointsCounter.points;
            CompleteSlider.GetComponent<Slider>().value = (PointsCounter.points / (float)PointsCounter.needPoints);
        }
    }
}