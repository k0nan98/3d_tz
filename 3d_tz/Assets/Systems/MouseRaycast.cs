using Leopotam.Ecs;

using UnityEngine;
namespace Client {

    sealed class MouseRaycast : IEcsRunSystem {
        public static Vector3 HitPosition;
        Vector3 hitPos;
        // auto-injected fields.
        readonly EcsWorld _world = null;

        
        void IEcsRunSystem.Run () {
#if UNITY_EDITOR
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitPoint;

            int layerMask = 1 << 8;
            layerMask = ~layerMask;
            if (Physics.Raycast(ray, out hitPoint, 100, layerMask))
            {

                HitPosition = hitPoint.point;
            }
#elif UNITY_ANDROID
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            RaycastHit hitPoint;

            int layerMask = 1 << 8;
            layerMask = ~layerMask;
            if (Physics.Raycast(ray, out hitPoint, 100, layerMask))
            {

                HitPosition = hitPoint.point;
            }
#endif
        }
    }
}