using Leopotam.Ecs;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace Client {
    sealed class PointsCounter : IEcsInitSystem, IEcsDestroySystem {
        // auto-injected fields.
        readonly EcsWorld _world = null;
        CompositeDisposable disposables;

        public static int points = 0;
        public static int needPoints;
        public void Init()
        {
            disposables = new CompositeDisposable();
            GameObject[] blocks = GameObject.FindGameObjectsWithTag("Blocks");
            needPoints = blocks.Length;
            foreach (GameObject block in blocks)
            {
                block.GetComponent<Rigidbody>().OnDestroyAsObservable().Subscribe(_ => { incPoints(); }).AddTo(disposables);
            }

        }
        public static bool GetGameResult()
        {
            if (PointsCounter.points >= PointsCounter.needPoints) return true;
            else return false;
        }
        void incPoints()
        {
            points++;
        }


        public void Destroy()
        {
            if(disposables != null)
            disposables.Dispose();

        }
    }
}