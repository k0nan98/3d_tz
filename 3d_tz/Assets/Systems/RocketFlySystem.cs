using Leopotam.Ecs;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Client {
    sealed class RocketFlySystem : IEcsRunSystem, IEcsDestroySystem, IEcsInitSystem {
        // auto-injected fields.
        readonly EcsWorld _world = null;
        CompositeDisposable disposables;
        GameObject[] rockets;
        LevelSetting currentSettings;
        public static void StartRocket()
        {
            GameObject sample = GameObject.FindGameObjectWithTag("RocketSample");
            GameObject newRocket = GameObject.Instantiate(sample,sample.transform.position, sample.transform.rotation);
            if (newRocket != null)
            {
                newRocket.GetComponent<CapsuleCollider>().enabled = true;
                newRocket.GetComponent<TrailRenderer>().enabled = true;
                newRocket.tag = "Rocket";
            }
        }
        void IEcsRunSystem.Run () {
            /*
            rockets = GameObject.FindGameObjectsWithTag("Rocket");
            if (rockets.Length > 0)
            {
                foreach (GameObject rocket in rockets)
                {
                    rocket.transform.parent = null;
                    rocket.GetComponent<Rigidbody>().AddForce(rocket.transform.forward, ForceMode.Acceleration);
                    disposables = new CompositeDisposable();
                    rocket.GetComponent<Rigidbody>().OnCollisionEnterAsObservable().Subscribe(_ => { RocketBoom(rocket); }).AddTo(disposables);
                }
            }*/
        }
        void RocketBoom(GameObject rocket)
        {
            rocket.GetComponent<Rigidbody>().AddExplosionForce(currentSettings.GetRocketExplosionForce(), rocket.transform.position, currentSettings.GetRocketExplosionRadius());
            UnityEngine.GameObject.Destroy(rocket.gameObject);
        }

        public void Destroy()
        {
            if (disposables != null)
            {
                disposables.Dispose();
            }
        }

        public void Init()
        {
            currentSettings = Resources.Load<LevelSetting>("Level_" + (Application.loadedLevel));
        }
    }
}